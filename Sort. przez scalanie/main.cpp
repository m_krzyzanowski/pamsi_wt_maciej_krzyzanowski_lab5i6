#include <iostream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include <fstream>

using namespace std;

template <typename T>
void merge(T tablica[], int lewy, int srodkowy, int prawy)
{
    T *wyjscie = new T[(prawy-lewy)+1];
    int l = lewy;
    int s = srodkowy + 1;

    int wsk = 0;

    while (l <= srodkowy && s <= prawy)
    {
        if (tablica[l] > tablica[s])
            wyjscie[wsk++] = tablica[s++];
        else
            wyjscie[wsk++] = tablica[l++];
    }

    while (l <= srodkowy)
        wyjscie[wsk++] = tablica[l++];

    while (s <= prawy)
        wyjscie[wsk++] = tablica[s++];

    //przkopiowanie posortowanej czesci tablicy
    for (int i = 0; i <= prawy-lewy; i++)
        tablica[lewy+i] = wyjscie[i];

    delete [] wyjscie;
}

template <typename T>
void mergeSort(T tablica[], int lewy, int prawy)
{
    if (lewy < prawy) //rekurencyjny podzial tablice (krok podstawowy: tablice jednoelementowe),
    {
        int srodkowy = (lewy + prawy) / 2; //zmienna przchowuje indeks pierwszego elementu
        mergeSort(tablica, lewy, srodkowy);
        mergeSort(tablica, (srodkowy + 1), prawy);
        merge(tablica, lewy, srodkowy, prawy); //procedura scalania tablic
    }
}

int main()
{
    fstream plik;
    plik.open("plik_tekstowy.txt",ios::out);

    LARGE_INTEGER frequency;        // ticks per second
    LARGE_INTEGER t1, t2;           // ticks
    double elapsedTime;

    int ile = 1000000; //ilosc elementow do pososrotwania
    int proce = ile * 0; //ilosc pososrotwanych element�w

    //dynamiczna alokacja tablicy
    int *tablica=new int [ile];
    int *bufor = new int [ile];

    //inicjowanie generatora
    srand(time(NULL));

   for(int i=0; i<100; i++)
    {
        //sortowanie calej tablicy i odwracanie kolejnosci
        for(int k=0; k<ile; k++)
            bufor[k] = rand()%1000000+1;

        mergeSort(bufor, 0, ile-1);

        for(int l=0, m=ile-1; l<ile && m>0; l++, m--)
            tablica[l] = bufor[m];

        // get ticks per second
        QueryPerformanceFrequency(&frequency);

        // start timer
        QueryPerformanceCounter(&t1);

        /////////////////////////////////////////////////////////////////////////////////////////////
        mergeSort(tablica, 0, ile-1);
        /////////////////////////////////////////////////////////////////////////////////////////////

        // stop timer
        QueryPerformanceCounter(&t2);

        // compute and print the elapsed time in millisec
        elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
        cout << elapsedTime << " ms.\n";

         //prawdzenie poprawnosci posortowania
        int licznik = 1;
        while(licznik < ile)
        {
            if(tablica[licznik-1] > tablica[licznik])
            {
                 cout << "W sortowaniu wystapil blad." << endl;
                 cout << licznik;
            }
            licznik++;
        }

        plik << elapsedTime << endl;

    }

    plik.close();

    return 0;
}





